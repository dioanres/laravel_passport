<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commentmodel extends Model
{
    public $table = 'comments';

    public $fillable = [
        'id',
        'id_news',
        'comment'
    ];

    public function news() {
        //$this->belongsTo(\App\Modules\Master\Entities\EmployeesModel::class,'id_role','role_id');
        return $this->belongsTo(\App\Models\NewsModel::class,'id','id_news');
    }

}
