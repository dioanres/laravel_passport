<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsModel extends Model
{
    public $table = 'news';

    public $fillable = [
        'id',
        'title',
        'content',
        'image',
        'path_image',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    protected $primaryKey = 'id';

    public function comments(){
        return $this->hasMany(\App\Models\Commentmodel::class,'id_news');
    }
}
