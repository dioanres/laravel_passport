<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    public $table = 'activity_log';

    public $fillable = [
        'id',
        'subject',
        'url',
        'method',
        'ip',
        'agent',
        'user_id',
    ];
}
