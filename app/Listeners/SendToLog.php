<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\NewsWasCreated;
use LogActivity;

class SendToLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewsWasCreated $event)
    { 
        $news = $event->news;
        $action = $event->action;   
        LogActivity::addToLog($action);
        //Log::info("Product was Created, product name: $news->title");
    }
}
