<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\NewsRepository;
use Illuminate\Support\Facades\Storage;
use App\Events\NewsWasCreated;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(NewsRepository $newsRepo)
    {
        $this->newsRepository = $newsRepo; 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function listData()
    {
        //dd($this->newsRepository->all());
        return $this->newsRepository->with('comments')->paginate(5);
    }

    public function save(Request $request)
    {
        $data = $request->all();
        $image = $request->file('image')->store('public/news');
        $data['image'] = $image;
        $news = $this->newsRepository->create($data);
        event(new NewsWasCreated($news,'Create News'));
        return 'true';
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $image = $request->file('image')->store('public/news');
        $data['image'] = $image;
        $update = $this->newsRepository->updateById($id,$data);
        event(new NewsWasCreated($update,'Update News'));
        return $update;
    }

    public function delete($id) {
        $delete = $this->newsRepository->deleteById($id); 
        event(new NewsWasCreated(null,'Delete News'));
        return 'true';
    }
}
