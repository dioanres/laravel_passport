<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CommentRepository;

class CommentController extends Controller
{
    public function __construct(CommentRepository $commentRepo)
    {
        $this->commentRepository = $commentRepo; 
        
    }

    public function listData() {
        return $this->commentRepository->all();
    }

    public function save(Request $request)
    {
        $data = $request->all();
        
        return $this->commentRepository->create($data);
    }

    public function delete($id) {
        $delete = $this->commentRepository->deleteById($id); 
        return true;
    }
}
