how to install
1. git clone : https://gitlab.com/dioanres/laravel_passport.git
2. composer install
3. change file .env.example -> .env and set DB in .env -> database_name : laravel_test
4. run 'php artisan key:generate'
5. install passport run 'php artisan passport:install'
6. run 'php artisan migrate'
7. run aplikasi with  'php artisan serve'
8. signup user for take a token for route and parameter please see below.
6. after signup please login and get token
7. copy token in autentication postman or insomnnia for doing CRUD News
8. enjoy it.


Users
    - Signup
        * API_URL : api/signup
        * param_name : name,email, password
    - Login
        * API_URL : api/Login
        * param_name : email,password

News
    - create (POST)
        * API_URL : api/news/save
        * Param_name : title,content, image
    - update (POST)
        * API_URL : api/news/update
        * Param_name : id,title,content, image
    - list with comment (GET)
        * API_URL : api/news/listData
    - delete (GEt)
        * API_URL : api/news/delete/{id}

Comment 
    - create (POST)
        * API_URL : api/comment/save
        * param_name : id_news, comment