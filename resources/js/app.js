
require('./bootstrap')

import Vue from 'vue'
import router from './routes'

const app = new Vue({
    el: '#app',
    router
});
