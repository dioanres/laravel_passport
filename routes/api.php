<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login','AuthController@login');
Route::post('signup','AuthController@signup');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('news')->group(function () {
    Route::get('/listData','NewsController@listData')->name('news.listData');
});

Route::middleware('auth:api')->group(function(){
    Route::get('details','AuthController@details');

    Route::prefix('news')->group(function () {
        //Route::get('/listData','NewsController@listData')->name('news.listData');
        Route::post('/save','NewsController@save')->name('news.save');
        Route::post('/update','NewsController@update')->name('news.update');
        Route::get('/delete/{id}','NewsController@delete')->name('news.delete');
    });
});

Route::prefix('comment')->group(function () {
    Route::get('/listData','CommentController@listData')->name('comment.listData');
    Route::post('/save','CommentController@save')->name('comment.save');
    Route::post('/update','CommentController@update')->name('comment.update');
    Route::get('/delete/{id}','CommentController@delete')->name('comment.delete');
});

// Route::group(
//     ['prefix' => 'news',
//     'middleware' => 'auth:api'
//     ], function () {
//     Route::get('/listData', 'NewsController@listData')->name('news.listData'); 
// });

